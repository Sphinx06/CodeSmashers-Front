// ##############################################################################
// # TEST FRONT-END -- CYPRESS
// # CIBLE : LISTJET.VUE 
// ##############################################################################

//==========================================
// Ajouts de commandes CYPRESS
//==========================================
// Commande permettant d'uploader une contenu sur un input[type=file]
Cypress.Commands.add('upload_file', (selector, fixturePath, filename, mimeType = ' ') => {
  return cy.get(selector)
    .then((subject) => {
      cy.fixture(fixturePath, 'base64').then((base64String) => {
        Cypress.Blob.base64StringToBlob(base64String, mimeType)
          .then(function (blob) {
            var testfile = new File([blob], filename, { type: mimeType })
            var dataTransfer = new DataTransfer()
            var fileInput = subject[0]

            dataTransfer.items.add(testfile)
            // This triggers the @change event on the input.
            fileInput.files = dataTransfer.files

            cy.wrap(subject).trigger('change', { force: true })
          })
      })
    })
})

//==========================================
// TESTS
//==========================================

context("ListJet : Jetpack Home Page", () => {
  beforeEach(() => {
    cy.visit("/")
  })

  it("Check if page show all jetpacks", () => {
    cy.contains("Jetpack Fortnite Wiki")
    cy.contains("Jetpack JackTalior")
    cy.get(".jetpackCard").should('have.length', 2)
  })

  it("Add new jetpack", () => {
    // Input du nom du jetpack
    cy.get("#name").type('Martin Jetpack V12')

    // Input de l'image
    const selector = "input[type=file]"
    const fixturePath = 'logo.png' // contenu dans le dossier de cypress/fixtures 
    const mimeType = 'image/png'
    const filename = "logo.png"
    cy.upload_file(selector, fixturePath, filename, mimeType)

    // Ajout du jetpack
    cy.get("#btnAddJet").click()
    cy.get(".v-snack").should('be.visible')
    cy.get(".jetpackCard").should('have.length', 3)
  })

  it("Checking we cannot create a jetpack with wring data", () => {
    // Tentative sans input 
    cy.get("#btnAddJet").should('have.attr', 'disabled', "disabled")
    cy.get("#btnAddJet").click({ force: true })
    cy.get(".jetpackCard").should('have.length', 2)

    // Tentative avec nom trop long sans image
    cy.get("#name").type("12345678901234567890123456789012345678901")
    cy.get("#btnAddJet").should('have.attr', 'disabled', "disabled")
    cy.get("#btnAddJet").click({ force: true })
    cy.get(".jetpackCard").should('have.length', 2)

    // Tentative sans image avec nom correct
    cy.get("#name").type("Martin Jetpack P12")
    cy.get("#btnAddJet").should('have.attr', 'disabled', "disabled")
    cy.get("#btnAddJet").click({ force: true })
    cy.get(".jetpackCard").should('have.length', 2)

    // Tentative avec image sans nom
    const selector = "input[type=file]"
    const fixturePath = 'logo.png' // contenu dans le dossier de cypress/fixtures 
    const mimeType = 'image/png'
    const filename = "logo.png"
    cy.upload_file(selector, fixturePath, filename, mimeType)
    cy.get("#name").clear()
    cy.get("#btnAddJet").should('have.attr', 'disabled', "disabled")
    cy.get("#btnAddJet").click({ force: true })
    cy.get(".jetpackCard").should('have.length', 2)
  })

})
