import httpClient from '../../src/httpClient'

describe('httpClient', () => {
  let payload = { toto: 'test' }
  beforeEach(() => {
    window.fetch = jest.fn(() =>
      Promise.resolve({ json: () => Promise.resolve('test') })
    )
  })

  it('should send a fetch request', async () => {
    const res = await httpClient.fetch('/test', 'POST', payload)

    expect(window.fetch).toHaveBeenCalledWith('/test', {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST',
      body: JSON.stringify(payload)
    })
    expect(res).toBe('test')
  })

  it('should send a get request', async () => {
    httpClient.fetch = jest.fn()
    await httpClient.get('/test')

    expect(httpClient.fetch).toHaveBeenCalledWith('/test', 'GET')
  })

  it('should send a post request', async () => {
    httpClient.fetch = jest.fn()
    await httpClient.post('/test', payload)

    expect(httpClient.fetch).toHaveBeenCalledWith('/test', 'POST', payload)
  })

  it('should send a put request', async () => {
    httpClient.fetch = jest.fn()
    await httpClient.put('/test', payload)

    expect(httpClient.fetch).toHaveBeenCalledWith('/test', 'PUT', payload)
  })

  it('should send a delete request', async () => {
    httpClient.fetch = jest.fn()
    await httpClient.delete('/test/1')

    expect(httpClient.fetch).toHaveBeenCalledWith('/test/1', 'DELETE')
  })
})
