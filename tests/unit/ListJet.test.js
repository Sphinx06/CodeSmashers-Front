import { shallowMount, createLocalVue } from '@vue/test-utils'
import { SilenceWarnHack } from './ressources/SilenceWarnHack.js'
import fetchMock from 'fetch-mock'
import flushPromises from 'flush-promises'
import Vuetify from 'vuetify'
import ListJet from '../../src/components/ListJet.vue'

/*********************** 
* Gestion des constantes
*********************** */
const silenceWarnHack = new SilenceWarnHack()

const getResult = [{
  id: "123",
  name: "The Jetpack",
  image: "base64 ..."
}]

const postResult = {
  id: "1243",
  name: "The Jetpacksdfsdf",
  image: "base64 sdfsf..."
}

/*********************** 
* Gestion de la vue
*********************** */
silenceWarnHack.enable()
const localVue = createLocalVue()
localVue.use(Vuetify)
silenceWarnHack.disable()


/*********************** 
* Test du composant
*********************** */
describe('ListJet', () => {
  let wrapper

  beforeEach(async () => {
    fetchMock.get('/jetpacks', getResult, { overwriteRoutes: true })
    fetchMock.post('/jetpacks', postResult, { overwriteRoutes: true })
    const pickFile = jest.fn()
    wrapper = shallowMount(ListJet, { localVue, pickFile })
    await flushPromises()
  })

  // TESTS RENDER
  // Vérification du rendu du composant.
  it('ListJet : Checking component render.', async () => {
    expect(wrapper.find('#titleToolbar').text()).toEqual('Liste des jetpacks')
    expect(wrapper.name()).toBe('ListJet')
  })

  it('ListJet : Testing initial data.', async () => {
    expect(wrapper.vm.snackbar).toBe(false)
    expect(wrapper.vm.snackbarColor).toEqual("success")
    expect(wrapper.vm.snackbarMsg).toEqual("")
    expect(wrapper.vm.snackbarTimeout).toEqual(5000)
    expect(wrapper.vm.valid).toBe(false)
    expect(wrapper.vm.imageName).toEqual('')
    expect(wrapper.vm.imageUrl).toEqual('')
    expect(wrapper.vm.imageFile).toEqual('')
    expect(wrapper.vm.errorMsg).toEqual('')
  })

  // TESTS GETJETPACK
  // Vérification de la récupération des jetpacks après mount.
  it('ListJet : getJetpack after mount.', async () => {
    // Vérification de la présence d'un jetpack dans les datas
    expect(wrapper.vm.jetpacks).toEqual(getResult)
    // Vérification de l'affichage du jetpack
    expect(wrapper.find('.jetpackTitle').text()).toEqual("The Jetpack")
  })


  // TESTS ADDJETPACK ET FORMULAIRE
  // Vérification de l'ajout d'un jetpack sans inputs.
  it('ListJet : addJetpack without input/initialization', async () => {
    // Vérification du message d'erreur
    expect(wrapper.vm.errorMsg).toEqual("")
    // Déclenchement du click forcé
    wrapper.find('#btnAddJet').vm.$emit('click')
    await flushPromises()
    // Vérification du message d'erreur
    expect(wrapper.vm.errorMsg).toEqual("Veuillez attribuer un nom au jetpack de moins de 40 caractères.")
    expect(wrapper.vm.snackbarColor).toEqual("error")

    // Vérification du message d'erreur
    wrapper.setData({ name: null })
    // Déclenchement du click forcé
    wrapper.find('#btnAddJet').vm.$emit('click')
    await flushPromises()
    // Vérification du message d'erreur
    expect(wrapper.vm.errorMsg).toEqual("Veuillez attribuer un nom au jetpack de moins de 40 caractères.")
    expect(wrapper.vm.snackbarColor).toEqual("error")
  })

  // Vérification de l'ajout d'un jetpack sans inputs.
  it('ListJet : addJetpack with data', async () => {
    wrapper.setData({ name: "TEST", imageUrl: "TEST" })
    wrapper.find('#btnAddJet').vm.$emit('click')
    await flushPromises()

    // Vérification de la data
    expect(wrapper.vm.errorMsg).toEqual("")
    expect(wrapper.vm.jetpacks).toEqual(expect.arrayContaining([expect.objectContaining({ id: "1243" })]))
    expect(wrapper.vm.snackbarColor).toEqual("success")
    expect(wrapper.vm.snackbarMsg).toEqual("Le jetpack a été ajouté.")
  })

    // Vérification de l'ajout d'un jetpack sans inputs.
    it('ListJet : addJetpack test catch with throwing error in fetch by forcing jetpack to null', async () => {
      wrapper.setData({ name: "TEST", imageUrl: "TEST" })
      wrapper.setData({ jetpacks: null})
      wrapper.find('#btnAddJet').vm.$emit('click')
      await flushPromises()
  
      // Vérification de la data
      expect(wrapper.vm.snackbarColor).toEqual("error")
    })

  // Vérification de l'ajout d'un nom de jetpack supérieur à 40 caractères
  it('ListJet : addJetpack with methods validation data', async () => {
    wrapper.setData({ name: "123456789012345678901234567890123456789011", imageUrl: "TEST" })
    expect(wrapper.vm.isValid).toEqual(false)
    wrapper.$nextTick
    wrapper.setData({ name: "1234567890123456789012345678901234567890", imageUrl: "" })
    expect(wrapper.vm.isValid).toEqual(false)
    
    wrapper.setData({ name: "123456789012345678901234567890123456789011", imageUrl: "" })
    expect(wrapper.vm.isValid).toEqual(false)
    
    wrapper.setData({ name: "AZZAZAZAZAZAZZAZZZZZZZZZZZZZZZZZ", imageUrl: null })
    expect(wrapper.vm.isValid).toEqual(false)
    
    wrapper.setData({ name: null, imageUrl: "" })
    expect(wrapper.vm.isValid).toEqual(false)
  })


  // Vérification de l'ajout d'un nom de jetpack supérieur à 40 caractères
  it('ListJet : addJetpack testing front-end name input rules', async () => {
    expect(wrapper.vm.nameRules[0](wrapper.vm.name)).toEqual("Nom obligatoire")
    wrapper.setData({ name: "123456789012345678901234567890123456789011" })
    expect(wrapper.vm.nameRules[1](wrapper.vm.name)).toEqual("Le nom doit contenir moins de 40 caractères")
    wrapper.setData({ name: "123456789012345678901234567890123456789" }) // 40 PILE CARACTERES
    expect(wrapper.vm.nameRules[0](wrapper.vm.name)).toEqual(true)
    expect(wrapper.vm.nameRules[1](wrapper.vm.name)).toEqual(true)
  })


  it("ListJet : File upload ", async () => {


    // Définition variables et mocks
    const fileContents = 'Ceci est le contenu du fichier image'
    const file = new File([new ArrayBuffer(2e+5)], 'image.jpeg', { lastModified: null, type: 'image/jpeg' })
    const readAsDataURL = jest.fn()
    const addEventListener = jest.fn((_, evtHandler) => { evtHandler() })
    const dummyFileReader = { addEventListener, readAsDataURL, result: fileContents }
    window.FileReader = jest.fn(() => dummyFileReader)
    let fileInput = { files: [file] }
    let ev = { type: 'change', target: fileInput }

    // Lancement de la fonction
    wrapper.vm.onFilePicked(ev)

    // Vérification des appels interne à la fonction 
    expect(FileReader).toHaveBeenCalled()
    expect(addEventListener).toHaveBeenCalled()

    // Vérification de la mise à jour de la data 
    expect(wrapper.vm.imageFile).toEqual(file)
    expect(wrapper.vm.imageUrl).toEqual(fileContents)
  })

  it("ListJet : File upload invalid file extension ", async () => {

    // Définition variables et mocks
    const fileContents = 'Ceci est le contenu du fichier image'
    const file = new File([new ArrayBuffer(2e+5)], 'wrongFilenameWithoutExt', { lastModified: null, type: 'image/jpeg' })
    const readAsDataURL = jest.fn()
    const addEventListener = jest.fn((_, evtHandler) => { evtHandler() })
    const dummyFileReader = { addEventListener, readAsDataURL, result: fileContents }
    window.FileReader = jest.fn(() => dummyFileReader)
    let fileInput = { files: [file] }
    let ev = { type: 'change', target: fileInput }

    // Lancement de la fonction
    wrapper.vm.onFilePicked(ev)

    // Vérification des appels interne à la fonction 
    expect(FileReader).not.toHaveBeenCalled()
    expect(addEventListener).not.toHaveBeenCalled()

    // Vérification de la mise à jour de la data 
    expect(wrapper.vm.imageFile).toEqual('')
    expect(wrapper.vm.imageUrl).toEqual('')
  })

  it("ListJet : File upload without file", async () => {

    // Définition variables et mocks
    const fileContents = 'Ceci est un contenu vide'
    const file = undefined
    const readAsDataURL = jest.fn()
    const addEventListener = jest.fn((_, evtHandler) => { evtHandler() })
    const dummyFileReader = { addEventListener, readAsDataURL, result: fileContents }
    window.FileReader = jest.fn(() => dummyFileReader)
    let fileInput = { files: [file] }
    let ev = { type: 'change', target: fileInput }

    // Lancement de la fonction
    wrapper.vm.onFilePicked(ev)

    // Vérification des appels interne à la fonction 
    expect(FileReader).not.toHaveBeenCalled()
    expect(addEventListener).not.toHaveBeenCalled()

    // Vérification de la mise à jour de la data 
    expect(wrapper.vm.imageFile).toEqual('')
    expect(wrapper.vm.imageUrl).toEqual('')
  })

  it("ListJet : PickFile ", async () => {
    // Définition variables et mocks
    const click = jest.fn()
    wrapper.vm.$refs = {
      image: {
        click
      }
    }

    // Lancement de la fonction
    wrapper.vm.pickFile()

    // Vérification des appels interne à la fonction 
    expect(click).toHaveBeenCalled()
  })

  it("ListJet: Snackbar check", async () => {

    const color = "error"
    const msg = "hello"
    // Lancement de la fonction
    wrapper.vm.showSnackbar(color,msg)

    // Vérification des appels interne à la fonction 
    expect(wrapper.vm.snackbarColor).toEqual(color)
    expect(wrapper.vm.snackbarMsg).toBe(msg)
    expect(wrapper.vm.snackbar).toBe(true)
  })

})
