import { shallowMount } from '@vue/test-utils'
import DateTime from '@/components/DateTime'
import Vue from 'vue'
import Vuetify from 'vuetify'
import ParentComponent from '@/components/DateSearch'
Vue.use(Vuetify)
jest.mock('@/utils')

let wrapper = shallowMount( DateTime , {
    propsData: {
      labeldate: 'Date de début',
      labelhour: 'Heure de début',
      idhour: 'hour1',
      iddate:'date'
    }
  })
  console.log(wrapper)
describe('DateTime.vue', () => {
 


  it('test props', async () => {
    expect(wrapper.props().labeldate).toBe('Date de début')
    expect(wrapper.props().labelhour).toBe('Heure de début')
    expect(wrapper.props().idhour).toBe('hour1')
    expect(wrapper.props().iddate).toBe('date')

  })

  
  it('test emit', () => {
    const button = wrapper.find("#hourpicker")
    button.trigger('change')
    expect(wrapper.vm.menu2).toBe(false)
    wrapper.vm.$emit('selected', '00:00')
    expect(wrapper.emitted().selected).toBeTruthy()
    expect(wrapper.emitted().selected[0]).toEqual(['00:00'])
    
    
  })
  it('test emit Date', () => {
    wrapper.vm.emitdate('2019-06-06')

    expect(wrapper.vm.menu1).toBe(false)

    
    wrapper.vm.$emit('selectedd', '2019-06-06')
    expect(wrapper.emitted().selectedd).toBeTruthy()
    expect(wrapper.emitted().selectedd[0]).toEqual(['2019-06-06'])
    
    
  }) 
  it('test emit Hour',()=> {
    wrapper.vm.emitHour('00:00')

    console.log(wrapper.emitted())
    expect(wrapper.vm.menu2).toBe(false)

    
    wrapper.vm.$emit('selected', '00:00')
    expect(wrapper.emitted().selected).toBeTruthy()
  })



})
describe('ParentComponent', () => {
    it("displays 'Emitted!' when custom event is emitted", () => {
      const wrapper2 = shallowMount(ParentComponent)
      wrapper2.find(DateTime).vm.$emit('selectedd', '2019-06-06')
      expect(wrapper2.vm.date).toBe('2019-06-06' )
    })
    it("displays 'Emitted!' when custom event is emitted", () => {
        const wrapper2 = shallowMount(ParentComponent)
        wrapper2.find(DateTime).vm.$emit('selected', '00:00')
        expect(wrapper2.vm.hour).toBe('00:00' )
      })
  })
