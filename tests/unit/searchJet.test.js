import { shallowMount, createLocalVue } from "@vue/test-utils"
import { SilenceWarnHack } from "./ressources/SilenceWarnHack.js"
import httpClient from "@/httpClient"
import flushPromises from "flush-promises"
import Vuetify from "vuetify"
import dateSearch from "../../src/components/DateSearch.vue"
import ChildComponent from "@/components/DateTime.vue"
import moment from "moment-timezone"
//import moment from 'moment-timezone'
/***********************
 * Gestion des constantes
 *********************** */
const silenceWarnHack = new SilenceWarnHack()

/***********************
 * Gestion de la vue
 *********************** */
silenceWarnHack.enable()
const localVue = createLocalVue()
localVue.use(Vuetify)
silenceWarnHack.disable()
jest.mock("@/httpClient")
jest.mock("@/utils")

/***********************
 * Test du composant
 *********************** */
describe("dateSearch", () => {
  let wrapper

  beforeEach(async () => {
    httpClient.get = jest.fn(() =>
      Promise.resolve([
        { id: "1", name: "Toto" },
        { id: "2", name: "Tata" },
        { id: "3", name: "Titi" }
      ])
    )
    const pickFile = jest.fn()
    wrapper = shallowMount(dateSearch, { localVue, pickFile })
    await flushPromises()
  })

  // TESTS RENDER
  // Vérification du rendu du composant.
  it("dateSearch : Checking component render.", async () => {
    expect(wrapper.find("#titleDate").text()).toEqual("Recherche par date")
  })

  it("dateSearch : Testing initial data.", async () => {
    expect(wrapper.vm.snackbar).toBe(false)
    expect(wrapper.vm.snackbarColor).toEqual("success")
    expect(wrapper.vm.snackbarMsg).toBeNull()
    expect(wrapper.vm.snackbarTimeout).toEqual(5000)
    expect(wrapper.vm.names).toEqual([])
    expect(wrapper.vm.jetpacks).toEqual([])
    expect(wrapper.vm.availabilities).toEqual([])
    expect(wrapper.vm.date).toBe(new Date().toISOString().substr(0, 10))
    expect(wrapper.vm.dateEnd).toBe(new Date().toISOString().substr(0, 10))
    expect(wrapper.vm.labelDateStart).toEqual("Date de début")
    expect(wrapper.vm.labelDateEnd).toEqual("Date de fin")
    expect(wrapper.vm.labelHourStart).toEqual("Heure du début")
    expect(wrapper.vm.labelHourEnd).toEqual("Heure de fin")
    expect(wrapper.vm.hourClass1).toEqual("hour1")
    expect(wrapper.vm.hourClass2).toEqual("hour2")
    expect(wrapper.vm.hour).toBeNull()
    expect(wrapper.vm.hourEnd).toBeNull()
  })

  // TESTS ADDJETPACK ET FORMULAIRE
  // Vérification de l'ajout d'un jetpack sans inputs.
  it("dateSearch : addJetpack without input/initialization", async () => {
    // Vérification du message d'erreur
    // Déclenchement du click forcé
    wrapper.find("#submit").trigger("click")
    await flushPromises()
    expect(wrapper.find("#snackBar").isVisible()).toBe(true)
    // Vérification du message d'erreur
    expect(wrapper.vm.snackbar).toBe(false)

    // Vérification du message d'erreur
  })

  it("missing parameters", () => {
    wrapper.find("#submit").trigger("click")

    expect(httpClient.get).not.toHaveBeenCalled()
  })

  describe("ParentComponent", () => {
    it("check DateTime component emitDate ", () => {
      wrapper.find(ChildComponent).vm.$emit("selectedd", "2019-06-06")
      expect(wrapper.vm.date).toBe("2019-06-06")
    })
    it("check DateTime component emitHour", () => {
      wrapper.find(ChildComponent).vm.$emit("selected", "00:00")
      expect(wrapper.vm.hour).toBe("00:00")
    })
    it("test form", () => {
      wrapper.find(ChildComponent).vm.$emit("selected", "00:00")
      wrapper.find('[test="btnSubmit"]').trigger("click")
      expect(httpClient.get).not.toHaveBeenCalled()
    })
    it("test small functions", () => {
      wrapper.setData({
        date: "2019-06-06",
        dateEnd: "2019-06-06",
        hour: "01:00",
        hourEnd: "05:00"
      })
      wrapper.vm.changeDateStart("2019-06-06")
      expect(wrapper.vm.date).toBe("2019-06-06")
      wrapper.vm.changeDateEnd("2019-06-06")
      expect(wrapper.vm.dateEnd).toBe("2019-06-06")
      wrapper.vm.changeHour("00:00")
      expect(wrapper.vm.hour).toBe("00:00")
      wrapper.vm.changeHourEnd("00:00")
      expect(wrapper.vm.hourEnd).toBe("00:00")
      wrapper.vm.showSnackbar("error", "test")
      expect(wrapper.vm.snackbarColor).toBe("error")
      expect(wrapper.vm.snackbarMsg).toBe("test")
      expect(wrapper.vm.snackbar).toBe(true)

      wrapper.find(ChildComponent).vm.$emit("selected", "00:00")
      wrapper.find('[test="btnSubmit"]').trigger("click")
      expect(httpClient.get).not.toHaveBeenCalled()
    })
    it("test fonction form", () => {
      wrapper.setData({
        date: "2019-06-06",
        dateEnd: "2019-06-06",
        hour: "01:00",
        hourEnd: "05:00"
      })
      wrapper.vm.validate()

      expect(wrapper.vm.formatDateStart).toBe(
        moment("2019-06-06 01:00").format()
      )
      expect(wrapper.vm.formatDateEnd).toBe(
        moment("2019-06-06 05:00").format()
      )
    })

    it("test fonction form", () => {
      wrapper.setData({
        date: "2019-02-02",
        dateEnd: "2019-06-06",
        hour: "01:00",
        hourEnd: "05:00"
      })
      wrapper.vm.validate()
      expect(wrapper.vm.formatDateStart).toBe(
        moment("2019-02-02 01:00").format()
      )
      expect(wrapper.vm.formatDateEnd).toBe(
        moment("2019-06-06 05:00").format()
      )
      expect(wrapper.vm.errorMsg).toBe("Veuillez entrer des dates valides")
    })
    it("test fonction form invalid date syntax", () => {
      wrapper.setData({
        date: "2019-06-06",
        dateEnd: "2019-06-06",
        hour: "  +01:00",
        hourEnd: "05:00"
      })
      wrapper.vm.validate()

      expect(wrapper.vm.formatDateStart).toBe("Invalid date")
      expect(wrapper.vm.formatDateEnd).toBe(
        moment("2019-06-06 05:00").format()
      )
      expect(wrapper.vm.errorMsg).toBe("Veuillez entrer des dates valides")
    })
    it("test fonction form invalid date time", () => {
      wrapper.setData({
        date: "2019-06-07",
        dateEnd: "2019-06-06",
        hour: "01:00",
        hourEnd: "05:00"
      })
      wrapper.vm.validate()

      expect(wrapper.vm.formatDateStart).toBe(
        moment("2019-06-07 01:00").format()
      )
      expect(wrapper.vm.formatDateEnd).toBe(
        moment("2019-06-06 05:00").format()
      )
      expect(wrapper.vm.errorMsg).toBe("Veuillez entrer des dates valides")
    })
  })
})
