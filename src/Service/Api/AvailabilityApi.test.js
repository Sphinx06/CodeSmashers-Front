const AvailabilityApi = require('./AvailabilityApi')
const Availability = require('../../Entity/Availability')

describe('AvailabilityAPI get Availabilities', function () {

    test('Test GetAvailabilities', () => {
        let httpClientMock = {
            fetch: jest.fn()
        }

        httpClientMock.fetch.mockResolvedValue([
            {
                jetpack_id: "123",
                date_start: "2018-2-18",
                date_end: "2019-3-9"
            }
        ])

        let availabilityApiMock = new AvailabilityApi(httpClientMock)
        availabilityApiMock.getAvailabilities().then(resp => {
            expect(Array.isArray(resp)).toBe(true)
            expect(resp.length).toBe(1)
            expect(resp[0]).toBeInstanceOf(Availability)
        })
    })
})