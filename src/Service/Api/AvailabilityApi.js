const AvailabilityApi = require('../../Entity/Availability')
module.exports = class  {
    constructor (httpClient) {
        this.httpClient = httpClient
    }

    getAvailabilities () {
        return this.httpClient.fetch('/availabilities', {}).then(rows => {

            return rows.map(row => {
                let availability = new AvailabilityApi()
                availability.jetpack_id = row.jetpack_id
                availability.date_start = row.date_start
                availability.date_end = row.date_end
                return availability
            })
        })
    }
}