import Vue from "vue"
import Router from "vue-router"
import Home from "./views/Home.vue"
import Fetch from "./views/Fetch.vue"
import Search from "./views/SearchJetPack.vue"

Vue.use(Router)

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/fetch",
      name: "fetch",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: Fetch
    },
    {
      path: "/search",
      name: "search",
      component: Search
    }
  ]
})
