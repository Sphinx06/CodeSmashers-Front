module.exports = class  {
    constructor () {
        this._jetpack_id = null
        this._date_start = null
        this._date_end = null
    }

    get date_end () {
        return this._date_end
    }

    set date_end (value) {
        this._date_end = value
    }
    get date_start () {
        return this._date_start
    }

    set date_start (value) {
        this._date_start = value
    }
    get jetpack_id () {
        return this._jetpack_id
    }

    set jetpack_id (value) {
        this._jetpack_id = value
    }
}