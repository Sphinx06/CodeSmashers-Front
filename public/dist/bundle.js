/******/ (function (modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {}
  /******/
  /******/ 	// The require function
  /******/ 	function __webpack_require__ (moduleId) {
    /******/
    /******/ 		// Check if module is in cache
    /******/ 		if (installedModules[moduleId]) {
      /******/ 			return installedModules[moduleId].exports
      /******/
    }
    /******/ 		// Create a new module (and put it into the cache)
    /******/ 		var module = installedModules[moduleId] = {
      /******/ 			i: moduleId,
      /******/ 			l: false,
      /******/ 			exports: {}
      /******/
    }
    /******/
    /******/ 		// Execute the module function
    /******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__)
    /******/
    /******/ 		// Flag the module as loaded
    /******/ 		module.l = true
    /******/
    /******/ 		// Return the exports of the module
    /******/ 		return module.exports
    /******/
  }
  /******/
  /******/
  /******/ 	// expose the modules object (__webpack_modules__)
  /******/ 	__webpack_require__.m = modules
  /******/
  /******/ 	// expose the module cache
  /******/ 	__webpack_require__.c = installedModules
  /******/
  /******/ 	// define getter function for harmony exports
  /******/ 	__webpack_require__.d = function (exports, name, getter) {
    /******/ 		if (!__webpack_require__.o(exports, name)) {
      /******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter })
      /******/
    }
    /******/
  }
  /******/
  /******/ 	// define __esModule on exports
  /******/ 	__webpack_require__.r = function (exports) {
    /******/ 		if (typeof Symbol !== 'undefined' && Symbol.toStringTag) {
      /******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' })
      /******/
    }
    /******/ 		Object.defineProperty(exports, '__esModule', { value: true })
    /******/
  }
  /******/
  /******/ 	// create a fake namespace object
  /******/ 	// mode & 1: value is a module id, require it
  /******/ 	// mode & 2: merge all properties of value into the ns
  /******/ 	// mode & 4: return value when already ns object
  /******/ 	// mode & 8|1: behave like require
  /******/ 	__webpack_require__.t = function (value, mode) {
    /******/ 		if (mode & 1) value = __webpack_require__(value)
    /******/ 		if (mode & 8) return value
    /******/ 		if ((mode & 4) && typeof value === 'object' && value && value.__esModule) return value
    /******/ 		var ns = Object.create(null)
    /******/ 		__webpack_require__.r(ns)
    /******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value })
    /******/ 		if (mode & 2 && typeof value !== 'string') for (var key in value) __webpack_require__.d(ns, key, function (key) { return value[key] }.bind(null, key))
    /******/ 		return ns
    /******/
  }
  /******/
  /******/ 	// getDefaultExport function for compatibility with non-harmony modules
  /******/ 	__webpack_require__.n = function (module) {
    /******/ 		var getter = module && module.__esModule
    /******/ ? function getDefault () { return module['default'] }
    /******/ : function getModuleExports () { return module }
    /******/ 		__webpack_require__.d(getter, 'a', getter)
    /******/ 		return getter
    /******/
  }
  /******/
  /******/ 	// Object.prototype.hasOwnProperty.call
  /******/ 	__webpack_require__.o = function (object, property) { return Object.prototype.hasOwnProperty.call(object, property) }
  /******/
  /******/ 	// __webpack_public_path__
  /******/ 	__webpack_require__.p = ''
  /******/
  /******/
  /******/ 	// Load entry module and return exports
  /******/ 	return __webpack_require__(__webpack_require__.s = './index.js')
  /******/
})
/************************************************************************/
/******/({

  /***/ './app.config.js':
  /*! ***********************!*\
  !*** ./app.config.js ***!
  \***********************/
  /*! no static exports found */
  /***/ function (module, exports) {
    eval('module.exports = {\n    apiUrl : "http://localhost:4545"\n};\n\n//# sourceURL=webpack:///./app.config.js?')

    /***/
  },

  /***/ './index.js':
  /*! ******************!*\
  !*** ./index.js ***!
  \******************/
  /*! no static exports found */
  /***/ function (module, exports, __webpack_require__) {
    eval("const appConfig = __webpack_require__(/*! ./app.config */ \"./app.config.js\");\nconst JetpackService = __webpack_require__(/*! ./src/Service/Api/JetpackApi */ \"./src/Service/Api/JetpackApi.js\");\nconst HttpClient = __webpack_require__(/*! ./src/HttpClient */ \"./src/HttpClient.js\");\n\nconst httpClient = new HttpClient(appConfig.apiUrl);\nconst jetpackService = new JetpackService(httpClient);\n\n\njetpackService.getJetpacks().then(jetpacks => {\n    let html =  '';\n    jetpacks.forEach((jetpack) => {\n        html +=\n            '<div class=\"card\" style=\"width: 18rem;\">\\n' +\n            '  <img src=\"'+ jetpack.image +'\" class=\"card-img-top\" alt=\"...\">\\n' +\n            '  <div class=\"card-body\">\\n' +\n            '    <h5 class=\"card-title\">' + jetpack.name + '</h5>\\n' +\n            '    <a href=\"#\" class=\"btn btn-primary\">Edit</a>\\n' +\n            '  </div>\\n' +\n            '</div>'\n\n    });\n\n    document.getElementById('jetpacks').innerHTML = html;\n});\n\n//# sourceURL=webpack:///./index.js?")

    /***/
  },

  /***/ './src/Entity/Jetpack.js':
  /*! *******************************!*\
  !*** ./src/Entity/Jetpack.js ***!
  \*******************************/
  /*! no static exports found */
  /***/ function (module, exports) {
    eval('module.exports = class  {\n    constructor() {\n        this._id = null;\n        this._name = null;\n        this._image = null;\n    }\n\n    get id() {\n        return this._id;\n    }\n\n    set id(value) {\n        this._id = value;\n    }\n\n    get name() {\n        return this._name;\n    }\n\n    set name(value) {\n        this._name = value;\n    }\n\n    get image() {\n        return this._image;\n    }\n\n    set image(value) {\n        this._image = value;\n    }\n}\n\n//# sourceURL=webpack:///./src/Entity/Jetpack.js?')

    /***/
  },

  /***/ './src/HttpClient.js':
  /*! ***************************!*\
  !*** ./src/HttpClient.js ***!
  \***************************/
  /*! no static exports found */
  /***/ function (module, exports) {
    eval('module.exports = class  {\n    constructor(url) {\n        this.url = url;\n    }\n\n    fetch (path, options) {\n        return fetch(this.url + path, options).then(response => response.json());\n    }\n};\n\n//# sourceURL=webpack:///./src/HttpClient.js?')

    /***/
  },

  /***/ './src/Service/Api/JetpackApi.js':
  /*! ***************************************!*\
  !*** ./src/Service/Api/JetpackApi.js ***!
  \***************************************/
  /*! no static exports found */
  /***/ function (module, exports, __webpack_require__) {
    eval("const JetpackApi = __webpack_require__(/*! ../../Entity/Jetpack */ \"./src/Entity/Jetpack.js\");\nmodule.exports = class {\n    constructor(httpClient) {\n        this.httpClient = httpClient;\n    }\n\n    getJetpacks() {\n        return this.httpClient.fetch('/jetpacks', {}).then(rows => {\n\n            return rows.map(row => {\n                let jetpack = new JetpackApi();\n                jetpack.id = row.id;\n                jetpack.name = row.name\n                jetpack.image = row.image;\n                return jetpack\n\n\n\n            });\n        });\n    }\n};\n\n//# sourceURL=webpack:///./src/Service/Api/JetpackApi.js?")

    /***/
  }

  /******/
})
