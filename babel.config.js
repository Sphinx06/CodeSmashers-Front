module.exports = function (api) {
  api.cache(true)

  const presets = [
    [
      "@vue/app",
      {
        polyfills: ["es6.promise", "es6.symbol"]
      }
    ]
  ]
  const plugins = ["@babel/plugin-syntax-dynamic-import"]

  return {
    presets,
    plugins
  }
}
