```  
  _____          _       _____                     _                     ______               _   
 / ____|        | |     / ____|                   | |                   |  ____|             | |  
| |     ___   __| | ___| (___  _ __ ___   __ _ ___| |__   ___ _ __ ___  | |__ _ __ ___  _ __ | |_ 
| |    / _ \ / _` |/ _ \\___ \| '_ ` _ \ / _` / __| '_ \ / _ \ '__/ __| |  __| '__/ _ \| '_ \| __|
| |___| (_) | (_| |  __/____) | | | | | | (_| \__ \ | | |  __/ |  \__ \ | |  | | | (_) | | | | |_ 
 \_____\___/ \__,_|\___|_____/|_| |_| |_|\__,_|___/_| |_|\___|_|  |___/ |_|  |_|  \___/|_| |_|\__|
                                                                                                  
                                                                                                  
```                                                                                                 

# SETUP

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
## Run App
```
npm run serve
```

# TESTS
All the tests file are included in the path <rootDir>/tests
- UNIT files test are in the following path: <rootDir>/tests/unit/
- UI files test are in the following path: <rootDir>/tests/ui
- Mountebank/mock files are in the following path : <rootDir>/tests/mock/

The file 'vue.config.js' at project's root determine the port use for API:
- 4545 ==> mountebank tests
- 3000 ==> back end related project

### Lints and fixes files
```
npm run lint
```

### Run tests on VueJs App/Components
```
npm run test:unit
```

### Run tests on VueJs App/Components with coverage
```
npm run test:unitCoverage
```

### Run mutation tests
```
npm run test:unitMutation
```

### Run UI tests
```
npm run test:ui
```
